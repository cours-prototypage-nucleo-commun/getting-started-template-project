# Getting started : STM32 Template Project (RTFM)
___
## Introduction

This project is a template, empty project for STM32F4xx Nucleo boards. 

This includes project files for the GNU ARM GCC toolchain inside the Eclipse IDE. 
___
## Pre-requisites 
**(setup time ~40minutes, depending on your internet connection)**

Install the whole toolset : 

### Eclipse IDE
You will need a development environment to code, build, run, debug your projects. Eclipse is a well established, free to use IDE, made with Java, initially for Java development, but it today supports many other languages. 

Install [Eclipse for C/C++](https://eclipse.org/downloads/packages/eclipse-ide-cc-developers/lunasr2).

### GNU ARM Eclipse plugin
Install the [GNU ARM Eclipse plugin](http://gnuarmeclipse.livius.net/blog/plugins-install/) :

The recommended way to install these plug-ins is to use the Eclipse standard install/update mechanism: In the Eclipse menu: **Help → Install New Software…**

- in the Install window, click the **Add…** button (on future updates, just select the URL in the Work with: combo)
- fill in Name: with **GNU ARM Eclipse Plug-ins**
- fill in Location: with **http://gnuarmeclipse.sourceforge.net/updates**
- click the **OK** button

Then you'll only need :

- GNU ARM C/C++ Cross Compiler
- GNU ARM C/C++ OpenOCD Debugging
- GNU ARM C/C++ STM32Fx Project Templates

So only select them and click **Next >**

### GCC ARM Toolchain
The GNU ARM Eclipse Plugin adds functionality to Eclipse to properly configure and debug a C/C++ ARM project. But you still need to have the cross compiler 

Download the latest [GCC ARM tool-chain](https://launchpad.net/gcc-arm-embedded) for your platform and **have them in a identified location** (you'll need it when configuring eclipse)

Note for linux 64 bit users : check [this](http://gnuarmeclipse.livius.net/blog/toolchain-install/). Since the executables for linux are 32bit, you'll need to install more packages to have the compatibility.

### OpenOCD 
OpenOCD is a tool that allows the communication with many JTAG probes from many embedded platforms. As it could be furtherly explained [here](http://gnuarmeclipse.github.io/openocd/install/), it is very complex and does not come with pre-built binaries. Thans again to the GNU ARM Eclipse project, we now can have access to pre-built versions of openOCD and thankfully, they were built with ST-Link support, so we can use them with our STM32F401RE Nucleo boards.

The installation instructions are explained [here](http://gnuarmeclipse.github.io/openocd/install/), for your favorite platform.


### NOTES for Windows
If you're using Windows, the system might lack some build tools. 
You might also not have git installed. 

#### Git installation
Git is already installed  and well integrated in most Linux distributions and on recent Mac OSX versions. 

For windows users, a manual installation is needed. Go to [https://git-scm.com/download/win](https://git-scm.com/download/win) and install git.
NOTE : be sure, during the installation process, to select 

- “**Use Git from the Windows Command Prompt**” 
- “**Checkout as-is, commit as-is**”
- "**Use MinTTY (the default terminal of MSys2)**"

#### Windows build tools
On windows, you'll lack some basic GNU build tools. the GNU ARM Eclipse project has packaged them and they're available [here](http://gnuarmeclipse.github.io/windows-build-tools/install/). Install them

#### Windows drivers for STLink
Windows users should also install drivers for STLink support : [here](http://www.st.com/web/en/catalog/tools/PF260219)

## First setup

### Clone project
Clone this projet (the GIT link is at the top of this project webpage) using the command : git clone.

### Import project in Eclipse
Then import this project in Eclipse :

- File > Import > General > Existing Projects into Workspace
- Browse this this project's location on your disk
- You should see GettingStartedTemplateProject in the detected projects list -> check it
- Leave all other checkboxes unchecked

### Configure Eclipse Global Preferences
#### Global tools paths
Once we've installed the missing tools, we need to tell Eclipse where to find them. Let's do this in Eclipses Preferences :

- Go to **Window > Preferences** (**Eclipse > Preferences** for Mac users)
- then go in **C/C++ > Build > Global Tools Paths**
- Windows users : fill in the Build tools folder with the path where you installed the Windows build tools (e.g. `C:\Program Files\GNU ARM Eclipse\Build Tools\2.6-201507152002\bin`)
- fill in the Toolchain folder with the path where you've installed it (e.g. `C:\Program Files\GNU Tools ARM Embedded\4.9 2015q2\bin` for windows users)

#### Workspace Global settings (optional, but highly recommended)
Have a look [here](http://gnuarmeclipse.github.io/eclipse/workspace/preferences/) to have a more finely tuned Eclipse configuration. this will ease your development.

#### SourceTree
Once you master GIT with command line, or at least, have well understood its fundamentals, some GUI tools can help you have a better owerview on your projects. Many GUIs exist, few are very efficient. Sourcetree is one of the tools that are complete enough and quite compliant with GIT and its vocabulary.
Sourcetree is avaialable for Mac ans Windows.

#### Setup string substitutions
In eclipse preferences (**Eclipse > Preferences** on Mac OS; **Window > Preferences** on other platforms), go to **run/debug > string substitutions** and create/update to have :

| name                 | description                                           | comments                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  |
|----------------------|-------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| openocd_path         | the path to the folder containing OpenOCD executable  | e.g. : `/usr/local/bin` is de default installation path on a homebrew install on Mac OS. On windows, if you've installed from [here](), you'll probably want to use this path : `C:\Program Files\GNU ARM Eclipse\OpenOCD\0.9.0-201505190955\bin`. On a unix system, you can check your openocd installation by typing `openocd` in a terminal if something is returned, you have openocd installed (you should see its version number printed). In this case `which openocd` will give you its location. |
| openocd_executable   | openocd executable name                               | Should be `openocd.exe` on windows; `openocd` on other platforms                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| openocd_scripts_path | the path to the folder containing the openocd scripts | On mac : `/usr/local/share/openocd/scripts/`. On Windows: `C:\Program Files\GNU ARM Eclipse\OpenOCD\0.9.0-201505190955\scripts`                                                                                                                                                                                                                                                                                                                                                                           |
| openocd_f4_arguments | the arguments to be passed to openocd when walling it | on a 0.9.0 version of openOCD, it should be : `-f ./board/st_nucleo_f4.cfg` otherwise, check in your openocd scripts/board/ folder.                                                                                                                                                                                                                                                                                                                                                                       |
___

### Build project
If your environment is well installed, you should be all set to build your project. 
go to **Project > Build Project** to build the project

In the console tab, you should see an output showing the build result. If you see something like this : 

	arm-none-eabi-size --format=berkeley "GettingStartedTemplateProject.elf"
	text	   data	    bss	    dec	    hex	filename
	3736	   1084	   1568	   6388	   18f4	GettingStartedTemplateProject.elf
	Finished building: GettingStartedTemplateProject.siz
	
Then the build has gone successfully.

You can now run and debug your program on the Nucleo board : 

- go to **Run > Debug Configurations...**
- under **GDB OpenOCD Debugging**, you should see **`Debug GettingStartedTemplateProject on NUCLEO F4xx`**
- select it and click **Debug**

The connection must now happen with the board (you should see the LD1 led blinking red/green on it) and Eclipse will propose you to switch to debug view and halt at first line of main().

- resume operation by clicking **Run > Resume**
- the led is now blinking every second.

You're done!